/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lk_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/19 17:23:02 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:02:00 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_lklist			*lk_lst_new(void *content, size_t size)
{
	t_lklist		*new;

	if ((new = (t_lklist *)ft_memalloc(sizeof(t_lklist))))
	{
		if (content)
		{
			if ((new->content = ft_memalloc(size)))
			{
				ft_memcpy(new->content, content, size);
				new->size = size;
			}
		}
	}
	return (new);
}
