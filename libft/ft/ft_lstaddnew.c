/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstaddnew.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 20:32:52 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:01:30 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int				ft_lstaddnew(t_list **alst, void const *cntt, size_t cntt_size)
{
	t_list		*lst;

	if (!(lst = ft_lstnew(cntt, cntt_size)))
		return (0);
	ft_lstadd(alst, lst);
	return (1);
}
