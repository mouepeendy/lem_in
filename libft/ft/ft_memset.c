/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/20 19:05:13 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:04:32 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void				*ft_memset(void *b, int c, size_t len)
{
	unsigned char	*d;

	if (len == 0)
		return (b);
	d = (unsigned char *)b;
	while (len--)
	{
		*d = (unsigned char)c;
		d++;
	}
	return (b);
}
