/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/21 19:13:45 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:04:29 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int				ft_strcmp(const char *s1, const char *s2)
{
	if (*(unsigned char *)s1 == *(unsigned char *)s2 && *(unsigned char *)s1)
		return (ft_strcmp(s1 + 1, s2 + 1));
	return (*(unsigned char *)s1 - *(unsigned char *)s2);
}
