/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/07 19:40:55 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:05:36 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static t_list	*reverse(t_list *lst, t_list *prev)
{
	t_list		*tmp;

	tmp = lst;
	if (lst->next)
		lst = reverse(lst->next, lst);
	tmp->next = prev;
	return (lst);
}

t_list			*ft_lstrev(t_list *lst)
{
	return (reverse(lst, NULL));
}
