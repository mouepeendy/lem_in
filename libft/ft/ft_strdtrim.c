/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdtrim.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/22 17:20:37 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:04:28 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strdtrim(char const *s, char d)
{
	char		*trim;
	int			i;
	int			j;

	i = 0;
	j = ft_strlen(s) - 1;
	while (s[i] == d)
		i++;
	while (s[j] == d && j >= 0)
		j--;
	if (j < i)
		trim = ft_strnew(0);
	else
		trim = ft_strsub(s, i, j - i + 1);
	return (trim);
}
