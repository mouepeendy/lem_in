/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/13 21:08:02 by juepee-m          #+#    #+#             */
/*   Updated: 2018/04/20 23:20:10 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include <stdio.h>
#include <string.h>

static int		word_count(char const *s, char c)
{
	int			i;
	int			nb_word;

	i = 0;
	nb_word = 0;
	if (!s || !c)
		return (0);
	while (s[i])
	{
		if (s[i] != c && (s[i + 1] == c || s[i + 1] == '\0'))
			nb_word++;
		i++;
	}
	return (nb_word);
}

static int		word_len(char const *s, char c)
{
	int			nb_char;
	size_t		i;

	nb_char = 0;
	i = 0;
	while (s[i] && s[i] != c)
	{
		if (s[i] != c && s[i + 1] != c)
			nb_char++;
		i++;
	}
	nb_char++;
	return (nb_char);
}

char			**ft_strsplit(char const *s, char c)
{
	char		**tab;
	int			nb_char;
	int			i;
	int			j;

	i = -1;
	j = 0;
	if (!(tab = (char **)malloc(sizeof(char *) * (word_count(s, c) + 1))))
		return (NULL);
	while (++i < word_count(s, c))
	{
		while (s[j] && s[j] == c)
			j++;
		if (s[j] != c)
		{
			nb_char = word_len(&s[j], c);
			tab[i] = ft_strnew(sizeof(char) * (nb_char + 1));
			ft_strncpy(tab[i], &s[j], nb_char);
		}
		j += nb_char;
	}
	tab[i] = NULL;
	return (tab);
}
