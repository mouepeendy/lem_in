/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_at.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/22 10:45:27 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:02:01 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vector.h"

void		*vector_at(t_vector *v, unsigned int index)
{
	if (!v || index > v->count || !v->data)
		return (NULL);
	return (v->data[index]);
}
