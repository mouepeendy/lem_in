/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_delete.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 11:24:00 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:02:05 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vector.h"

int		vector_delete(t_vector *v, unsigned int index)
{
	unsigned int	i;
	unsigned int	j;

	if (!v || index > v->count || !v->data)
		return (0);
	i = index;
	j = index;
	while (i++ < v->count)
	{
		v->data[j] = v->data[i];
		j++;
	}
	v->count--;
	return (1);
}
