/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_free.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 11:23:57 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:02:06 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vector.h"

void		vector_free(t_vector *v, unsigned int index)
{
	if (!v || index > v->count || !v->data)
		return ;
	free(v->data[index]);
}
