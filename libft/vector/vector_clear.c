/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_clear.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/24 15:49:34 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:02:03 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vector.h"

void			vector_clear(t_vector *v)
{
	if (!v)
		return ;
	ft_bzero(v->data, sizeof(void *) * v->count);
	v->count = 0;
}
