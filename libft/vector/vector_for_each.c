/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_for_each.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 15:18:44 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:02:05 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vector.h"

void				vector_for_each(t_vector *v, void (*f)(void *))
{
	unsigned int	i;

	if (!v || !v->data)
		return ;
	i = 0;
	while (i < v->count)
	{
		f(v->data[i]);
		i++;
	}
}
