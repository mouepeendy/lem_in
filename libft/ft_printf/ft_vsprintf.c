/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vsprintf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/01 19:07:35 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:01:22 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int				ft_vsprintf(char *str, char *format, va_list ap)
{
	char		*out;
	int			pf_size;

	if (!str)
		return (-1);
	out = NULL;
	pf_size = ft_vasprintf(&out, format, ap);
	ft_memcpy(str, out, pf_size);
	free(out);
	return (pf_size);
}
