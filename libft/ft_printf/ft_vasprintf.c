/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vasprintf.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/01 18:53:14 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:01:21 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int				ft_vasprintf(char **ret, char *format, va_list ap)
{
	t_buffer	*buffer;
	int			pf_size;

	buffer = NULL;
	if (!format)
		return (-1);
	if (!(buffer = pf_create_buffer(&buffer)))
		return (-1);
	while (*format)
	{
		if (*format == '%')
			pf_parse(&format, ap, &buffer);
		else
			format += pf_fill_buffer(format, 1, &buffer);
	}
	pf_size = pf_join_buffer(buffer, ret);
	return (pf_size);
}
