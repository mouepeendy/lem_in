/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vsnprintf.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/01 19:09:07 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:01:22 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int				ft_vsnprintf(char *str, size_t size, char *format, va_list ap)
{
	char		*out;
	int			pf_size;

	out = NULL;
	pf_size = ft_vasprintf(&out, format, ap);
	if ((size_t)pf_size < size)
		size = pf_size;
	ft_memcpy(str, out, size);
	free(out);
	return (size);
}
