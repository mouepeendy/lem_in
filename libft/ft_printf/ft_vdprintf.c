/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vdprintf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/09 18:37:05 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:01:21 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int				ft_vdprintf(int fd, char *format, va_list ap)
{
	char		*out;
	int			pf_size;

	out = NULL;
	pf_size = ft_vasprintf(&out, format, ap);
	write(fd, out, pf_size);
	free(out);
	return (pf_size);
}
