/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemin.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdelory <mdelory@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 21:12:31 by mdelory           #+#    #+#             */
/*   Updated: 2019/10/03 17:59:30 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static void		print_data(void *data)
{
	ft_putendl((char *)data);
}

static void		del_room(void *room)
{
	free(((t_room *)room)->name);
}

static void		lem_free(t_lemin *lem)
{
	int			i;

	i = -1;
	free(lem->parent);
	free(lem->visited);
	while (++i < lem->nb_room)
	{
		free(lem->graph[i]);
		free(lem->flow[i]);
	}
	free(lem->graph);
	free(lem->flow);
	vector_for_each(&lem->v_room, del_room);
	vector_free_all(&lem->v_room);
	vector_free_all(&lem->v_data);
	vector_destroy(&lem->v_room);
	vector_destroy(&lem->v_data);
}

static int		lem_init(t_lemin *lem)
{
	lem->nb_ant = 0;
	lem->nb_room = 0;
	lem->visited = NULL;
	lem->parent = NULL;
	lem->flow = NULL;
	lem->graph = NULL;
	lem->batch = NULL;
	return (vector_init(&lem->v_room) && vector_init(&lem->v_data));
}

int				main(void)
{
	t_lemin		lem;

	if (lem_init(&lem) && lem_parse(&lem))
	{
		if (!(lem.visited = (int *)malloc(sizeof(int) * lem.nb_room)))
			return (0);
		if (!(lem.parent = (int *)malloc(sizeof(int) * lem.nb_room)))
			return (0);
		lem.flow = ft_matrix(lem.nb_room, lem.nb_room, 0);
		if (lem_solve(&lem) && lem.batch)
		{
			vector_for_each(&lem.v_data, print_data);
			ft_putendl("");
			if (!check_one_turn(&lem))
				lem_dispatch(&lem);
			del_batch(lem.batch);
		}
		else
			ft_dprintf(2, "[-]No path found\n");
	}
	lem_free(&lem);
	return (0);
}
