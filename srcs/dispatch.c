/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dispatch.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdelory <mdelory@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/24 18:31:03 by mdelory           #+#    #+#             */
/*   Updated: 2019/09/23 17:38:19 by mdelory          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static void		next_step(t_ant **ant_lst, int nants)
{
	int			i;

	i = 0;
	while (i < nants)
	{
		if (!ant_lst[i]->arrived)
			ant_lst[i]->pos++;
		i++;
	}
}

static int		print_ants(t_ant **ant_lst, int nants)
{
	int			i;
	int			arrived;
	t_ant		*a;

	i = 0;
	arrived = 0;
	while (i < nants)
	{
		a = ant_lst[i];
		if (!a->arrived)
		{
			ft_printf("L%d-%s ", a->id, a->path[a->pos].name);
			if (a->path[a->pos].flag == LM_END)
			{
				a->arrived = 1;
				arrived++;
			}
		}
		i++;
	}
	ft_printf("\n");
	return (arrived);
}

t_ant			*new_ant(int id, t_room *path)
{
	t_ant		*ant;

	if ((ant = (t_ant *)malloc(sizeof(t_ant))))
	{
		ant->id = id + 1;
		ant->path = path;
		ant->pos = 0;
		ant->arrived = 0;
	}
	return (ant);
}

static int		ft_free_ants(t_ant **ant_lst, int nb_ant)
{
	int			i;

	i = -1;
	while (++i < nb_ant)
		free(ant_lst[i]);
	free(ant_lst);
	return (1);
}

int				lem_dispatch(t_lemin *lem)
{
	t_ant		**ant_lst;
	int			arrived;
	int			i;
	int			j;

	if (!(ant_lst = malloc(sizeof(t_ant *) * lem->nb_ant)))
		return (0);
	i = 0;
	arrived = 0;
	while (arrived < lem->nb_ant)
	{
		j = -1;
		while (++j < lem->batch->nb_path && i < lem->nb_ant)
		{
			if (lem->batch->path_limit[j])
			{
				ant_lst[i] = new_ant(i, lem->batch->path_lst[j]);
				lem->batch->path_limit[j]--;
				i++;
			}
		}
		next_step(ant_lst, i);
		arrived += print_ants(ant_lst, i);
	}
	return (ft_free_ants(ant_lst, lem->nb_ant));
}
