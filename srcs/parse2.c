/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/02 19:08:17 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/24 21:39:19 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int					init_graph(t_lemin *lem, int r1, int r2)
{
	lem->graph[r1][r2] = 1;
	lem->graph[r2][r1] = 1;
	return (1);
}

int					display_arg(t_lemin *lem, char **str)
{
	int		ret;

	vector_add(&lem->v_data, *str);
	ret = get_next_line(0, str);
	return (ret);
}
