/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdelory <mdelory@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/19 04:18:28 by mdelory           #+#    #+#             */
/*   Updated: 2019/09/24 23:44:13 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static int		init_queue(t_lemin *lem, int *queue)
{
	int			i;
	int			j;

	i = 0;
	j = 0;
	while (i < lem->nb_room)
	{
		if (get_room_by_id(lem, i)->flag == LM_START)
		{
			queue[j++] = i;
			lem->visited[i] = 1;
		}
		i++;
	}
	return (j);
}

static void		clear_parents(t_lemin *lem)
{
	int			i;

	i = 0;
	while (i < lem->nb_room)
	{
		lem->parent[i] = -1;
		i++;
	}
}

static int		check_room(t_lemin *lem, int room, int n)
{
	int			parent;
	int			flow;

	flow = lem->flow[room][n];
	if (flow == 1)
		return (0);
	if (flow == 0 && lem->visited[n] & LM_FORWARD)
		return (0);
	if (flow == -1 && lem->visited[n] & LM_BACKWARD)
		return (0);
	parent = lem->parent[room];
	if (parent == n)
		return (0);
	if (lem->flow[room][n] == 0 && parent != -1 && \
			lem->flow[parent][room] == 0 && room_path(lem, room))
		return (0);
	return (flow == 0 ? LM_FORWARD : LM_BACKWARD);
}

static int		bfs(t_lemin *lem, int *queue, int qsize)
{
	int			i;
	int			r;
	int			n;
	int			f;

	i = 0;
	while (i < qsize)
	{
		r = queue[i];
		n = 0;
		while (n < lem->nb_room)
		{
			if (lem->graph[r][n] && (f = check_room(lem, r, n)))
			{
				lem->parent[n] = r;
				if (get_room_by_id(lem, n)->flag == LM_END)
					return (n);
				lem->visited[n] |= f;
				queue[qsize++] = n;
			}
			n++;
		}
		i++;
	}
	return (-1);
}

int				lem_solve(t_lemin *lem)
{
	t_batch		*b;
	int			i;
	int			*queue;
	int			end;

	b = NULL;
	if ((queue = (int *)malloc(sizeof(int) * lem->nb_room * 2)))
	{
		i = 0;
		end = 0;
		clear_parents(lem);
		ft_bzero(lem->visited, sizeof(int) * lem->nb_room);
		while (end != -1 && (end = bfs(lem, queue,
						init_queue(lem, queue))) != -1)
		{
			apply_flow(lem, end);
			i++;
			do_batch(lem, b, &end, i);
			clear_parents(lem);
		}
		free(queue);
	}
	return (1);
}
