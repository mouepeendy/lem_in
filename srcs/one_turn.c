/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   one_turn.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdelory <mdelory@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/23 17:23:18 by mdelory           #+#    #+#             */
/*   Updated: 2019/09/23 17:48:13 by mdelory          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static void		print_one_turn(int nb_ant, t_room *end)
{
	int			i;

	i = 0;
	while (++i < nb_ant)
		ft_printf("L%d-%s ", i, end->name);
	ft_printf("L%d-%s\n", i, end->name);
}

int				check_one_turn(t_lemin *l)
{
	int			i;

	i = 0;
	while (i < l->batch->nb_path)
	{
		if (l->batch->path_size[i] == 2)
		{
			print_one_turn(l->nb_ant, l->batch->path_lst[i] + 1);
			return (1);
		}
		i++;
	}
	return (0);
}
