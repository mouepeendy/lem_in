/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdelory <mdelory@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/18 18:33:08 by mdelory           #+#    #+#             */
/*   Updated: 2019/09/25 16:28:42 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static int			parse_ant(t_lemin *lem)
{
	char			*str;

	while (get_next_line(0, &str) > 0 && *str == '#')
	{
		vector_add(&lem->v_data, str);
	}
	vector_add(&lem->v_data, str);
	if (ft_strnum(str) > 0)
	{
		lem->nb_ant = ft_atoi(str);
		if ((unsigned int)lem->nb_ant < 256000)
			return (1);
	}
	ft_dprintf(2, "[-]Invalid Ant Number: %s\n", str);
	return (0);
}

static int			parse_link(t_lemin *lem, char **av)
{
	char			*sep;
	int				r1;
	int				r2;
	int				ret;

	ret = 0;
	if (av[0] && !av[1])
	{
		if ((sep = ft_strchr(av[0], '-')) && sep != av[0] && *(sep + 1))
		{
			*sep = 0;
			if ((r1 = get_room_id(lem, av[0])) == -1)
				ft_dprintf(2, "[-]Unknown room: %s\n", av[0]);
			else if ((r2 = get_room_id(lem, sep + 1)) == -1)
				ft_dprintf(2, "[-]Unknown room: %s\n", sep + 1);
			else if (r1 == r2)
				ft_dprintf(2, "[-]Self linked room: %s\n", av[0]);
			else
				ret = init_graph(lem, r1, r2);
			*sep = '-';
		}
	}
	ft_freetab(av);
	return (ret);
}

static int			parse_room(t_lemin *lem, char **av, int *flag)
{
	t_room			*r;

	r = NULL;
	if (av[0] && av[1] && av[2] && !av[3])
	{
		if (!ft_strchr(av[0], '-'))
		{
			if (ft_strnum(av[1]) && ft_strnum(av[2]))
			{
				if (get_room_id(lem, av[0]) != -1)
					ft_dprintf(2, "[-]Room already exist: %s\n", av[0]);
				else if ((r = create_room(av[0], ft_atoi(av[1]),
								ft_atoi(av[2]), *flag)))
					vector_add(&lem->v_room, r);
			}
			else
				ft_dprintf(2, "[-]Invalid coordinates\n");
		}
		else
			ft_dprintf(2, "[-]Invalid room name\n");
	}
	*flag = 0;
	ft_freetab(av);
	return (r != NULL);
}

static int			parse_comment(char *str, int *flag)
{
	if (str && *str == '#')
	{
		if (ft_strcmp(str, "##start") == 0)
			*flag = LM_START;
		else if (ft_strcmp(str, "##end") == 0)
			*flag = LM_END;
		return (1);
	}
	return (0);
}

int					lem_parse(t_lemin *lem)
{
	char		*str;
	int			flag;
	int			ret;

	flag = 0;
	if (parse_ant(lem) && get_next_line(0, &str) > 0)
	{
		while ((parse_comment(str, &flag) || \
					parse_room(lem, ft_strsplit(str, ' '), &flag)))
			ret = display_arg(lem, &str);
		lem->nb_room = vector_count(&lem->v_room);
		lem->graph = ft_matrix(lem->nb_room, lem->nb_room, 0);
		while (ret > 0 && (parse_comment(str, &flag) || \
					parse_link(lem, ft_strsplit(str, ' '))))
			ret = display_arg(lem, &str);
		free(str);
		if (ret < 0)
			return (0);
		return (1);
	}
	return (0);
}
