/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/02 19:11:27 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/02 19:14:06 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void			do_batch(t_lemin *lem, t_batch *b, int *end, int i)
{
	b = get_batch(lem, i);
	calc_batch(lem, b);
	ft_bzero(lem->visited, sizeof(int) * lem->nb_room);
	if (!lem->batch || b->time <= lem->batch->time)
	{
		if (lem->batch)
			del_batch(lem->batch);
		lem->batch = b;
	}
	else
	{
		del_batch(b);
		*end = -1;
	}
}

void			apply_flow(t_lemin *lem, int end)
{
	int			prev;

	while (get_room_by_id(lem, end)->flag != LM_START)
	{
		prev = lem->parent[end];
		lem->flow[end][prev] -= 1;
		lem->flow[prev][end] += 1;
		end = prev;
	}
}
