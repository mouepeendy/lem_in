/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   room.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdelory <mdelory@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/18 18:48:53 by mdelory           #+#    #+#             */
/*   Updated: 2019/08/07 18:46:10 by mdelory          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

t_room				*create_room(char *str, int x, int y, int flag)
{
	t_room			*new;

	if (!(new = (t_room *)malloc(sizeof(t_room))))
		return (NULL);
	if (!(new->name = ft_strdup(str)))
		return (NULL);
	new->flag = flag;
	new->x = x;
	new->y = y;
	return (new);
}

t_room				*get_room(t_lemin *lem, char *str)
{
	int				i;

	if ((i = get_room_id(lem, str) == -1))
		return (NULL);
	return ((t_room *)vector_at(&lem->v_room, i));
}

t_room				*get_room_by_id(t_lemin *lem, int i)
{
	if (i >= 0 && i < vector_count(&lem->v_room))
		return (vector_at(&lem->v_room, (unsigned int)i));
	return (NULL);
}

int					room_path(t_lemin *lem, int room)
{
	int			n;

	if (get_room_by_id(lem, room)->flag != LM_START)
	{
		n = 0;
		while (n < lem->nb_room)
		{
			if (lem->flow[room][n] != 0)
				return (1);
			n++;
		}
	}
	return (0);
}

int					get_room_id(t_lemin *lem, char *str)
{
	int				i;

	i = 0;
	while (i < lem->nb_room)
	{
		if (ft_strcmp(get_room_by_id(lem, i)->name, str) == 0)
			return (i);
		i++;
	}
	return (-1);
}
