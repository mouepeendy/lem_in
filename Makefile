# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mdelory <mdelory@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/11/16 20:47:09 by mdelory           #+#    #+#              #
#    Updated: 2019/10/03 17:15:06 by mdelory          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME := 		lem-in

SRC_DIR :=		./srcs
INC_DIR :=		./includes \
				./libft/includes
OBJ_DIR :=		./objs

SRC :=			lemin.c \
				parse.c \
				parse2.c\
				room.c \
				one_turn.c \
				solve.c \
				solve2.c \
				batch.c \
				dispatch.c

OBJ := 			$(SRC:%.c=$(OBJ_DIR)/%.o)

INC_FLAGS :=	$(addprefix -I, $(INC_DIR))
LIB_FLAGS :=	-Llibft -lft
CFLAGS +=		-g -Werror -Wall -Wextra

RM :=			rm -rf
MKDIR :=		mkdir -p

################################################################################
################################################################################

all: 			$(NAME)

$(OBJ_DIR)/%.o:	$(SRC_DIR)/%.c
	@$(MKDIR) $(dir $@)
	@echo "- Compiling $(NAME): $^"
	@$(CC) $(CFLAGS) $(INC_FLAGS) -c -o $@ $^

$(NAME):		clibft $(OBJ)
	@$(CC) $(OBJ) -o $(NAME) $(LIB_FLAGS)

.phony: clibft
clibft:
	@$(MAKE) -s -C libft

.phony: clean
clean:
	@$(MAKE) clean -s -C libft
	@$(RM) $(OBJ_DIR)

.phony: fclean
fclean:			clean
	@$(MAKE) fclean -s -C libft
	@$(RM) $(NAME)

.phony: re
re:				fclean all
